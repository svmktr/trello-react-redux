const initialState = {
    listCards: [],
    checklist: [],
    cardDetail: []
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case 'getListCards':
            return {
                ...state,
                listCards: payload
            }
        case 'checklists':
            return {
                ...state,
                checklist: payload
            }
        case 'cardDetail':
            return {
                ...state,
                cardDetail: payload
            }
        default:
            return state
    }
}
