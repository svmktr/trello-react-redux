import { takeLatest, all, fork, put, takeEvery } from 'redux-saga/effects'

const apiKey = '3ab31a41f3207fe5947ffa7e8e1edf70'
const token = 'c92dd04168c5dd0c1393cb038ce94b8b5fad1eb85516cb1ef705f01ed7a36b5c'
const boardId = '5e0b0a1f83f23a2fedcf209c'
const url = `https://api.trello.com/1/boards/${boardId}/lists?cards=all&key=${apiKey}&token=${token}`;

function* watcher() {
    yield takeEvery('GET_ALL', getAllData)
}

function* getAllData() {
    const json = yield fetch(url).then(res => res.json())
    yield put({ type: 'getListCards', payload: json })
}

function* watcher1() {
    yield takeLatest('ADD_CARD', addCard)
}

function* addCard(data) {
    const listId = data.listId
    const text = data.text
    const addCardUrl = `https://api.trello.com/1/cards?idList=${listId}&name=${text}&key=${apiKey}&token=${token}`;
    yield fetch(addCardUrl, { method: 'POST' })
    yield getAllData()
}


function* watcher2() {
    yield takeLatest('ADD_LIST', addList)
}

function* addList(data) {
    const text = data.text
    const addListUrl = `https://api.trello.com/1/lists?name=${text}&idBoard=${boardId}&pos=bottom&key=${apiKey}&token=${token}`
    yield fetch(addListUrl, { method: 'POST' })
    yield getAllData()
}



function* watcher3() {
    yield takeLatest('DEL_CARD', delCard)
}

function* delCard(data) {
    const cardId = data.cardId
    const deleteCardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;
    yield fetch(deleteCardUrl, { method: 'DELETE' })
    yield getAllData()
}


function* watcher4() {
    yield takeLatest('EDIT_CARD', editCard)
}

function* editCard(data) {
    console.log(data)
    const cardId = data.cardId
    const text = data.text
    const EditCardUrl = `https://api.trello.com/1/cards/${cardId}?name=${text}&key=${apiKey}&token=${token}`
    yield fetch(EditCardUrl, { method: 'PUT' })
    yield getAllData()
}

function* watcher5() {
    yield takeEvery('GET_CHECKLIST', getChecklist)
}

function* getChecklist(data) {
    // console.log(data)
    const cardId = data.cardId
    const checkListUrl = `https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=all&filter=all&fields=all&key=${apiKey}&token=${token}`;
    const json = yield fetch(checkListUrl).then(res => res.json()).then(data => data)
    yield put({ type: 'checklists', payload: json })
}

function* watcher6() {
    yield takeLatest('GET_CARD_DETAIL', getCardDetail)
}

function* getCardDetail(data) {
    // console.log(data)
    const cardId = data.cardId
    const cardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;
    const json = yield fetch(cardUrl).then(res => res.json()).then(data => data)
    yield put({ type: 'cardDetail', payload: json })

}

function* watcher7() {
    yield takeLatest('CHECK_STATE', checkState)
}

function* checkState(data) {
    // console.log(data)
    const cardId = data.cardId
    const itemId = data.itemId
    const state = data.state
    const UpdateCheckItemUrl = `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=${apiKey}&token=${token}`;
    yield fetch(UpdateCheckItemUrl, { method: 'PUT' })
    yield getChecklist(data)

}



function* watcher8() {
    yield takeLatest('ADD_CHECKLIST', addCheckList)
}

function* addCheckList(data) {
    const cardId = data.cardId
    const text = data.text
    const url = `https://api.trello.com/1/checklists?idCard=${cardId}&name=${text}&key=${apiKey}&token=${token}`;
    yield fetch(url, { method: 'POST' })
    yield getChecklist(data)

}



function* watcher9() {
    yield takeLatest('ADD_CHECKITEM', addcheckItem)
}

function* addcheckItem(data) {
    console.log(data)
    const checklistId = data.ckecklistId
    const text = data.text
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${text}&pos=bottom&checked=false&key=${apiKey}&token=${token}`;
    yield fetch(url, { method: 'POST' })
    yield getChecklist(data)
}

function* watcher10() {
    yield takeLatest('DELETE_CHECKLIST', deleteChecklist)
}

function* deleteChecklist(data) {
    const checklistId = data.checklistId
    const url = `https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${token}`;
    yield fetch(url, { method: 'DELETE' })
    yield getChecklist(data)
}


export default function* rootSaga() {
    yield all([
        fork(watcher),
        fork(watcher1),
        fork(watcher2),
        fork(watcher3),
        fork(watcher4),
        fork(watcher5),
        fork(watcher6),
        fork(watcher7),
        fork(watcher8),
        fork(watcher9),
        fork(watcher10)
    ])
}