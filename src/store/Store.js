import { createStore, applyMiddleware, compose } from "redux"
import createSagaMiddleware from 'redux-saga'
import rootRuducer from '../reducers/index'
import rootSaga from '../saga'


const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootRuducer, {}, compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))

sagaMiddleware.run(rootSaga)

export default store