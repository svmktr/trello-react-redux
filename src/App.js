import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Board from './componentes/Board'
import CheckList from './componentes/CheckList';

function App() {
  return (
    <Router>
      <div id='main'>
        <Route path='/' component={Board} />
        <Route path='/card/:cardId' component={CheckList} />
      </div>
    </Router>
  );
}

export default App;