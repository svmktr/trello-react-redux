import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkState } from '../actions/TrelloAction'


export class ChecklistItem extends Component {
    checkHandler = (cardId, itemId, state) => {
        state = state === 'complete' ? 'incomplete' : 'complete'
        this.props.checkState(cardId, itemId, state)
    }

    render() {
        // console.log(this.props.cardId)
        const { item, cardId, itemId } = this.props
        const state = this.props.item.state
        let checkStatus = false
        let textDecoration = 'None'
        if (state === 'complete') {
            checkStatus = true
            textDecoration = 'line-through'
        }
        return (
            <div style={{ display: 'flex', marginLeft: '15px', marginBottom: '-20px' }}>
                <input type="checkbox" checked={checkStatus} onChange={() => this.checkHandler(cardId, itemId, state)} />
                <p style={{ textDecoration: textDecoration, marginLeft: '12px' }}>{item.name}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    checkState,
}

export default connect(mapStateToProps, mapDispatchToProps)(ChecklistItem)
