import React, { Component } from 'react'
import Cards from './TrelloCards'
import AddBtn from './AddBtn'

export class TrelloList extends Component {
    render() {
        const { listName, cards, listId } = this.props
        return (
            <div className='list-div'>
                <div style={listStyle}>{listName}</div>
                {cards.map(card => (
                    <Cards cardName={card.name} key={card.id} cardId={card.id} />
                ))}
                <AddBtn listId={listId} />
            </div>

        )
    }
}

const listStyle = {
    margin: '10px',
    fontFamily: 'Arial, Helvetica, sans-serif'
}

export default TrelloList
