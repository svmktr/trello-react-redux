import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getListCards } from '../actions/TrelloAction'
import List from './TrelloList'
import AddBtn from './AddBtn'

export class Board extends Component {
    componentDidMount = async () => {
        await this.props.getListCards()
    }


    render() {
        return (
            <div className='main-div'>
                {(this.props.lists || []).map(item => (
                    <List listName={item.name} cards={item.cards} listId={item.id} key={item.id} />
                ))}
                <AddBtn list />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    lists: state.listCards
})

const mapDispatchToProps = {
    getListCards
}

export default connect(mapStateToProps, mapDispatchToProps)(Board)
