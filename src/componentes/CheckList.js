import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getCheckList, getCardDetail } from '../actions/TrelloAction'
import Checklists from './Checklists'
import AddcheckList from './AddChecklist'
import CloseIcon from '@material-ui/icons/Close';

export class CheckList extends Component {

    state = {
        name: '',
        desc: ''
    }

    componentDidMount = () => {
        const cardId = this.props.match.params.cardId
        this.props.getCheckList(cardId)
        this.props.getCardDetail(cardId)
    }

    componentDidUpdate(prevState) {
        if (prevState.cardDetail !== this.props.cardDetail) {
            this.setState({
                name: this.props.cardDetail.name,
                desc: this.props.cardDetail.desc
            })

        }


    }

    closeChecklist = () => {
        this.props.history.push('/')
    }


    render() {
        const cardId = this.props.match.params.cardId
        return (
            <div style={blur}>
                <div style={checkList}>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <p style={{ fontFamily: 'Arial, Helvetica, sans-serif', fontSize: '19px', fontWeight: '600' }}>{this.state.name}</p>
                        <CloseIcon style={{ cursor: 'pointer', backgroundColor: 'transparent' }} onClick={this.closeChecklist} />
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div>
                            <p style={{ fontFamily: 'Arial, Helvetica, sans-serif', fontSize: '19px', fontWeight: '500' }}>Decscription:</p>
                            <p style={{ marginTop: '0px', marginLeft: '18px', fontFamily: 'Arial, Helvetica, sans-serif', fontSize: '15px' }}>
                                {this.state.desc}
                            </p>
                        </div>
                        <AddcheckList list cardId={cardId} />
                    </div>

                    {(this.props.checklist || []).map(list => (
                        <Checklists items={list.checkItems} listName={list.name} key={list.id} cardId={cardId} checklistId={list.id} />
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    checklist: state.checklist,
    cardDetail: state.cardDetail
})


const mapDispatchToProps = {
    getCheckList,
    getCardDetail
}


const checkList = {
    display: 'flex',
    flexDirection: 'column',
    borderRadius: '3px',
    position: 'absolute',
    top: '40px',
    left: '15%',
    minHeight: '500px',
    width: '70%',
    backgroundColor: '#ebecf0',
    padding: '10px 17px 10px 17px'
}

const blur = {
    backgroundColor: 'rgba(0,0,0,0.7)',
    position: 'fixed',
    zIndex: '2',
    left: '0',
    top: '0',
    height: '100%',
    width: '100%',
    overflowY: 'auto',
}



export default connect(mapStateToProps, mapDispatchToProps)(CheckList)
