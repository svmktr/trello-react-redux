import React, { Component } from 'react'
import Card from '@material-ui/core/Card';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { delCard } from '../actions/TrelloAction'
import { connect } from 'react-redux'
import Textarea from 'react-textarea-autosize'
import { editCard } from '../actions/TrelloAction'
import CloseIcon from '@material-ui/icons/Close';
import { withRouter } from 'react-router-dom';


export class TrelloList extends Component {

    state = {
        formOpen: false,
        text: '',
        opecity: 1
    }


    desc = (e, cardId) => {
        this.props.history.push(`/card/${cardId}`)
        this.setState({ opecity: 0.5 })
    }


    delCard = (e, cardId) => {
        e.stopPropagation()
        this.props.delCard(cardId)
    }

    onKey = (e, cardId) => {
        if (e.key === 'Enter')
            this.submitHandler(cardId)
    }


    submitHandler = (cardId) => {
        this.props.editCard(cardId, this.state.text)
        this.setState({ formOpen: false })
    }

    handleChange = (e) => {
        this.setState({ text: e.target.value })
    }

    clickHandler = (e, cardId) => {
        e.stopPropagation()
        this.setState({ formOpen: true })
    }

    closeCard = (e) => {
        e.stopPropagation()
        this.setState({ formOpen: false })
    }

    renderForm = (cardId) => {
        return (
            <Card style={{ display: 'flex', padding: '8px 12px 4px 8px', marginTop: '8px', border: '1px solid gray', width: '180px', placeholder: 'Edit card' }}>
                <Textarea
                    onKeyPress={(e) => this.onKey(e, cardId)}
                    placeholder='Edit card'
                    autoFocus
                    onBlur={this.closeCard}
                    onChange={this.handleChange}
                    style={{ resize: 'none', width: '90%', border: 'none' }}
                />
                <CloseIcon onClick={this.closeCard} />
            </Card>
        )
    }


    editBtn = (cardId) => {
        return (
            <EditIcon className='edit-btn' style={editBtn} onClick={(e) => this.clickHandler(e, cardId)}></EditIcon>
        )
    }


    render() {
        const { cardName, cardId } = this.props
        return (
            <div className='list-div'>
                <Card className='cards' onClick={(e) => this.desc(e, cardId)}>
                    {cardName}
                    <DeleteIcon className='del-btn' style={styleForButton} onClick={(e) => this.delCard(e, cardId)}></DeleteIcon>
                    {this.state.formOpen ? this.renderForm(cardId) : this.editBtn(cardId)}
                </Card>
            </div>

        )
    }
}

const styleForButton = {
    fontSize: 'large'
};

const editBtn = {
    fontSize: 'medium'
};


const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    delCard,
    editCard
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TrelloList))


