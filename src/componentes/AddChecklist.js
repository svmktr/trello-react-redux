import React, { Component } from 'react'
import { connect } from 'react-redux'
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Textarea from 'react-textarea-autosize'
import Card from '@material-ui/core/Card';
import { addCheckList, addCheckItem } from '../actions/TrelloAction'

export class AddChecklist extends Component {

    state = {
        formOpen: false,
        text: ''
    }


    addListBtn = () => {
        const { list } = this.props
        const buttonText = list ? '+ Add checklist' : '+ Add item'
        const buttonOpacity = list ? 1 : 0.5
        const buttonTextColor = list ? 'white' : 'inherit'
        const buttonBackground = list ? 'black' : 'inherit'
        return (
            <div style={btnStyle}>
                <p style={{ opacity: buttonOpacity, color: buttonTextColor, background: buttonBackground, fontSize: '15px', marginTop: '0px' }} onClick={this.addList}>{buttonText}</p>
            </div>
        )
    }




    addList = () => {
        // console.log('addList');
        this.setState({ formOpen: true })
    }


    onKey = (e) => {
        if (e.key === 'Enter')
            this.submitHandler()
    }

    closeForm = (e) => {
        this.setState({ formOpen: false })
    }

    handleChange = (e) => {
        this.setState({ text: e.target.value })
    }

    submitHandler = () => {
        const { list, cardId, checklistId } = this.props
        if (list && this.state.text !== '') {
            this.props.addCheckList(cardId, this.state.text)
        }
        else if (this.state.text !== '') {
            this.props.addCheckItem(checklistId, this.state.text, cardId)
        }
        this.setState({
            text: '',
            formOpen: false
        })
    }



    renderForm = () => {
        const { list } = this.props
        const placeholder = list ? 'Enter checklist...' : 'Enter Item...'
        return (
            <div>
                <Card style={{ overflow: 'visible', minHeight: 30, maxWidth: 200, padding: '6px 8px 2px', marginBottom: '2%', marginLeft: 10 }}>
                    <Textarea
                        onKeyPress={this.onKey}
                        placeholder={placeholder}
                        autoFocus
                        onBlur={this.closeForm}
                        onChange={this.handleChange}
                        style={{ resize: 'none', width: '100%', border: 'none' }}
                    />
                </Card>
                <div style={buttonArea} >
                    <Button onMouseDown={this.submitHandler} varient='contained' style={{ color: 'white', backgroundColor: '#5aac44', fontSize: 11, height: 26, marginBottom: '8px' }}>+ add</Button>
                    <CloseIcon style={{ marginLeft: 10 }} onClick={this.closeForm}></CloseIcon>
                </div>
            </div>
        )
    }


    render() {
        return this.state.formOpen ? this.renderForm() : this.addListBtn()
    }
}



const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    addCheckList,
    addCheckItem
}


const btnStyle = {
    fontSize: 12,
    cursor: 'pointer',
    borderRadius: 3,
    height: 36,
    weight: 271,
    paddingLeft: 12
}

const buttonArea = {
    display: 'flex',
    alignItem: 'center',
    cursor: 'pointer',
    paddingLeft: 10,
    paddingTop: 5
}

export default connect(mapStateToProps, mapDispatchToProps)(AddChecklist)
