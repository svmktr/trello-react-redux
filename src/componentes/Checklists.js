import React, { Component } from 'react'
import { connect } from 'react-redux'
import ChecklistItem from './ChecklistItem'
import AddcheckList from './AddChecklist'
import { deleteChecklist } from '../actions/TrelloAction'
import CloseIcon from '@material-ui/icons/Close';



export class Checklists extends Component {


    deleteChecklist = (checklistId, cardId) => {

        this.props.deleteChecklist(checklistId, cardId)
    }

    render() {
        const { listName, items, cardId, checklistId } = this.props
        return (
            <div style={{ display: 'flex', justifyContent: 'space-evenly', flexDirection: 'column', marginTop: '35px' }}>
                <div style={{ display: 'flex', maxWidth: '300px', justifyContent: 'space-between' }}>
                    <div style={{ fontSize: '17px', fontWeight: '600' }}>{listName}</div>
                    <AddcheckList checklistId={checklistId} cardId={cardId} style={{ marginTop: '-10px' }} />
                    <CloseIcon style={{ backgroundColor: 'transparent', cursor: 'pointer' }} onClick={() => this.deleteChecklist(checklistId, cardId)} />
                </div>
                {items.map(item => (
                    <ChecklistItem item={item} key={item.id} cardId={cardId} itemId={item.id} />
                ))}

            </div>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    deleteChecklist

}

export default connect(mapStateToProps, mapDispatchToProps)(Checklists)
