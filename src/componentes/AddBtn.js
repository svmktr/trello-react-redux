import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addCard, addList } from '../actions/TrelloAction'
import CloseIcon from '@material-ui/icons/Close';
import Button from '@material-ui/core/Button';
import Textarea from 'react-textarea-autosize'
import Card from '@material-ui/core/Card';

export class AddBtn extends Component {

    state = {
        formOpen: false,
        text: ''
    }


    addListBtn = () => {
        const { list } = this.props
        const buttonText = list ? '+ Add another list' : '+ Add another card'
        const buttonOpacity = list ? 1 : 0.5
        const buttonTextColor = list ? 'white' : 'inherit'
        const buttonBackground = list ? 'rgba(0,0,0,.15)' : 'inherit'
        return (
            <div style={btnStyle}>
                <p style={{ opacity: buttonOpacity, color: buttonTextColor, background: buttonBackground }} onClick={this.addList}>{buttonText}</p>
            </div>
        )
    }




    addList = () => {
        // console.log('addList');
        this.setState({ formOpen: true })
    }


    onKey = (e) => {
        if (e.key === 'Enter')
            this.submitHandler()
    }

    closeForm = (e) => {
        this.setState({ formOpen: false })
    }

    handleChange = (e) => {
        this.setState({ text: e.target.value })
    }

    submitHandler = () => {
        const { list, listId } = this.props
        if (list && this.state.text !== '') {
            this.props.addList(this.state.text)
        }
        else if (this.state.text !== '') {
            this.props.addCard(listId, this.state.text)
        }
        this.setState({
            text: '',
            formOpen: false
        })
    }



    renderForm = () => {
        const { list } = this.props
        const placeholder = list ? 'Enter list title...' : 'Enter card title...'
        const buttonTitle = list ? 'Add card' : 'Add list'
        return (
            <div>
                <Card style={{ overflow: 'visible', minHeight: 30, maxWidth: 200, padding: '6px 8px 2px', marginBottom: '2%', marginLeft: 10 }}>
                    <Textarea
                        onKeyPress={this.onKey}
                        placeholder={placeholder}
                        autoFocus
                        onBlur={this.closeForm}
                        onChange={this.handleChange}
                        style={{ resize: 'none', width: '100%', border: 'none' }}
                    />
                </Card>
                <div style={buttonArea} >
                    <Button onMouseDown={this.submitHandler} varient='contained' style={{ color: 'white', backgroundColor: '#5aac44', fontSize: 11, height: 26, marginBottom: '8px' }}>{buttonTitle}</Button>
                    <CloseIcon style={{ marginLeft: 10 }} onClick={this.closeForm}></CloseIcon>
                </div>
            </div>
        )
    }


    render() {
        return this.state.formOpen ? this.renderForm() : this.addListBtn()
    }
}



const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {
    addCard,
    addList
}


const btnStyle = {
    fontSize: 12,
    cursor: 'pointer',
    borderRadius: 3,
    height: 36,
    weight: 271,
    paddingLeft: 12
}

const buttonArea = {
    display: 'flex',
    alignItem: 'center',
    cursor: 'pointer',
    paddingLeft: 10,
    paddingTop: 5
}

export default connect(mapStateToProps, mapDispatchToProps)(AddBtn)
