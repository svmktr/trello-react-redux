export const getListCards = () => (
    {
        type: 'GET_ALL'
    }
)

export const addCard = (listId, text) => (
    {
        type: 'ADD_CARD',
        listId,
        text       
    }
)

export const addList = (text) => (
    {
        type: 'ADD_LIST',
        text
    }
)

export const delCard = (cardId) => (
    {
        type: 'DEL_CARD',
        cardId
    }
)

export const editCard = (cardId, text) => (
    {
        type: 'EDIT_CARD',
        cardId,
        text
    }
)

export const getCheckList = (cardId) => (
    {
        type: 'GET_CHECKLIST',
        cardId,
    }
)


export const getCardDetail = (cardId) => (
    {
        type: 'GET_CARD_DETAIL',
        cardId,
    }
)

export const checkState = (cardId,itemId, state) => (
    {
        type: 'CHECK_STATE',
        cardId,
        itemId,
        state
    }
)


export const addCheckList = (cardId, text) => (
    {
        type: 'ADD_CHECKLIST',
        cardId,
        text
    }
)


export const addCheckItem = (ckecklistId, text, cardId) => (
    {
        type: 'ADD_CHECKITEM',
        ckecklistId,
        text, 
        cardId
    }
)


export const deleteChecklist = (checklistId, cardId) => (
    // console.log(checklistId,cardId )
    {
        type: 'DELETE_CHECKLIST',
        checklistId,
        cardId
    }
)